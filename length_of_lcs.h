//function to find the length of the longest common subsequence from the given two strings

int findlength(char *S,char *T,int n,int m)
{
    int i,j;
    
    for(i=0;i<=n;i++)
    {
        for(j=0;j<=m;j++)
        {
            if(i==0||j==0)
            {
                L[i][j]=0;
            }
            else if(S[i-1]==T[j-1])
            {
                L[i][j]=1+L[i-1][j-1];
            }
            else
            {
                L[i][j]=(L[i-1][j]>L[i][j-1])?(L[i-1][j]):(L[i][j-1]);
            }
        }
    }
    
    return L[n][m];
}    
    