//function displays the longest common subsequence of the 2 strings passed

void display_lcs(char *S, char *T, int n, int m, int len)
{
    char lcs[len+1];
    int i,j;
    
    lcs[len]='\0';
    i=n;
    j=m;
    
    while(i>0&&j>0)
    {
        if(S[i-1]=T[j-1])
        {
            lcs[len-1]=S[i-1];
            i--;
            j--;
            len--;
        }
        
        else if(L[i-1][j]>L[i][j-1])
        {
            i--;
        }
        
        else
        {
            j--;
        }
    }
    
    printf("\nLongest common subsequence of %s and %s is %s",S,T,lcs);
}